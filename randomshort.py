#!/usr/bin/env python3

import argparse
import http.server
import random
import string
import urllib
import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Acortador de URLs</title>
  </head>
  <body>
    <h1>Acortador de URLs</h1>
    <form method="POST">
      <label for="url">URL a acortar:</label>
      <input type="text" name="url" required>
      <br>
      <label for="resource_name">Nombre del recurso:</label>
      <input type="text" name="resource_name" required>
      <br>
      <input type="submit" value="Enviar">
      <br>
    </form>
    <h2>Listado de URLs acortadas:</h2>
    <br>
      <ul>
        {urls}
      </ul>
  </body>
</html>
"""

class shortApp(webapp.webApp):

    url_mapping = {}

    def parse(self, request):
        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\r\n\r\n', 1)[1]
        return (method, resource, body)

    def process(self, parsedRequest):
        method, resource, body = parsedRequest

        if method == "GET":
            if resource == "/":
                urls_html = "".join(f"<li><a href='/{resource}'>{resource}</a>: {url}</li>" for resource, url in self.url_mapping.items())
                page = PAGE.format(urls=urls_html)
                return ("200 OK", page)
            elif resource.startswith("/"):
                resource = resource[1:]
                if resource in self.url_mapping:
                    redirect_url=self.url_mapping[resource]
                    return ("301 Redirect", redirect_url)
                else:
                    return ("404 Not Found", "<html><body><h1>NOT FOUND!</h1></body></html>")
        elif method == "POST":
            preurl = urllib.parse.unquote(body.split('=')[1].split('&')[0])
            url = preurl if preurl.startswith(("https://", "http://")) else "http://" + preurl
            resource_name = urllib.parse.unquote(body.split('=')[2])

            self.url_mapping[resource_name] = url

            urls_html = "".join(f"<li><a href='/{resource}'>{resource}</a>: {url}</li>" for resource, url in self.url_mapping.items())
            page = PAGE.format(urls=urls_html)
            return ("200 OK", page)

if __name__ == "__main__":
    testShortApp = shortApp("localhost", 1234)